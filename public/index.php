<?php require_once '../resources/bootstrap.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<title>Otacon</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script type="text/javascript" src="./js/index.js" defer>
	</script>
</head>
<body>
<?php require_once 'templates/header.php'; ?>
<div class="wrapper">
	<div class="top-container">
	</div>
	<div class="content" id="content">
    <?php require_once 'views/content.php'; ?>
  </div>
</div>
</body>
</html>
