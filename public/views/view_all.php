<?php
require_once '../../resources/bootstrap.php';
require_once MODELS_PATH . 'Anime.php';

$type = '';

if(isset($_GET['type'])) {
  $type = $_GET['type'];
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>View All</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" href="../css/style.css">
  </head>
  <body>
    <?php require_once('../templates/header.php'); ?>
    <div class="content" id="content">
    <div class="cards-header" id="trending-container-header">
      <h3>TOP ANIME</h3>
      <div class="dropdown">
        <form method="get">
        <label for="dropdown" class="dropdown-label">filter type</label>
        <select name="type" id="trending-dropdown" class="form-group" onchange="this.form.submit()">
    		<option value="" selected="" disable="">Select filter type</option>
        <option id="tv" value="TV" <?php print $type === 'TV' ? 'selected' : ''; ?>>TV</option>
        <option id="movie" value="Movie" <?php print $type === 'Movie' ? 'selected' : ''; ?>>Movie</option>
        <option id="ova" value="OVA" <?php print $type === 'OVA' ? 'selected' : ''; ?>>OVA</option>
        <option id="special" value="Special" <?php print $type === 'Special' ? 'selected' : ''; ?>>Special</option>
        <option id="special" value="Music" <?php print $type === 'Music' ? 'selected' : ''; ?>>Music</option>
        <option id="special" value="ONA" <?php print $type === 'ONA' ? 'selected' : ''; ?>>ONA</option>
        </select>
      </form>
      </div>
    </div>
    <div class="card-container" id="view-top-container">
      <?php
        $anime = new Anime();
        $result = $anime->getTopAnime(50, $type);

        if (count($result) > 0) {
            foreach ($result as $row) {
                $id = htmlentities($row['id']);
                $title = htmlentities($row['title']);
                $imageId = htmlentities($row['image_id']);
                $viewAnimePath = URL_ROOT . "views/view_anime.php?id=$id";
                $viewImagePath = URL_ROOT . "views/view_image.php?image_id=$imageId";

                print "
            <div class=\"card\">
            <a href=\"$viewAnimePath\"><img class=\"card-img-container\" src=\"$viewImagePath\"></a>
              <p class=\"card-title\">$title</p>
            </div>";
            }
        }
      ?>
    </div>
  </div>
  </body>
</html>
