<?php require_once '../resources/bootstrap.php'; ?>
<?php require_once MODELS_PATH . 'Anime.php'; ?>
<div class="cards-header" id="trending-container-header">
  <h3>TOP ANIME</h3>
    <a href="<?php print URL_ROOT . 'views/view_all.php'; ?>">
      <p class="sort-control" id="view-all-trending">View All</p>
    </a>

</div>
<div class="card-container" id="trending-container">
<?php
  $anime = new Anime();

  $result = $anime->getTopAnime();

  if(count($result) > 0) {
    foreach ($result as $row) {
      $id = htmlentities($row['id']);
      $title = htmlentities($row['title']);
      $imageId = htmlentities($row['image_id']);
      $viewAnimePath = URL_ROOT . "views/view_anime.php?id=$id";
      $viewImagePath = URL_ROOT . "views/view_image.php?image_id=$imageId";

      print "
      <div class=\"card\">
      <a href=\"$viewAnimePath\"><img class=\"card-img-container\" src=\"$viewImagePath\"></a>
        <p class=\"card-title\">$title</p>
      </div>";
    }
  }
?>
</div>
<div class="cards-header" id="top-movies-container-header">
  <h3>TOP MOVIES</h3>
</div>
<div class="card-container" id="top-movies-container">
<?php
  $anime = new Anime();

  $result = $anime->getTopMovies();

  if(count($result) > 0) {
    foreach ($result as $row) {
      $id = htmlentities($row['id']);
      $title = htmlentities($row['title']);
      $imageId = htmlentities($row['image_id']);
      $viewAnimePath = URL_ROOT . "views/view_anime.php?id=$id";
      $viewImagePath = URL_ROOT . "views/view_image.php?image_id=$imageId";

      print "
      <div class=\"card\">
      <a href=\"$viewAnimePath\"><img class=\"card-img-container\" src=\"$viewImagePath\"></a>
        <p class=\"card-title\">$title</p>
      </div>";
    }
  }
?>
</div>
<div class="cards-header" id="upcoming-container-header">
   <h3>UPCOMING NEXT SEASON</h3>
</div>
<div class="card-container" id="upcoming-container">
  <?php
    $anime = new Anime();
    $result = $anime->getUpcomingAnime();

    if(count($result) > 0) {
      foreach ($result as $row) {
        $id = htmlentities($row['id']);
        $title = htmlentities($row['title']);
        $imageId = htmlentities($row['image_id']);
        $viewAnimePath = URL_ROOT . "views/view_anime.php?id=$id";
        $viewImagePath = URL_ROOT . "views/view_image.php?image_id=$imageId";

        print "
        <div class=\"card\">
        <a href=\"$viewAnimePath\"><img class=\"card-img-container\" src=\"$viewImagePath\"></a>
          <p class=\"card-title\">$title</p>
        </div>";
    }
  }
  ?>
  </div>
