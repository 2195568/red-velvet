<?php
require_once '../../resources/bootstrap.php';
require_once MODELS_PATH . 'Anime.php';


if(hasGetElement('search')) {
  $anime = new Anime();

  $sort = isset($_GET['sort']) ? $_GET['sort'] : '';
  $order = isset($_GET['order']) ? $_GET['order'] : '';

  $result = $anime->searchAnime($_GET['search'], $_GET['format'], $_GET['airing_status'], $_GET['genre'], $sort, $order);
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Search Result</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" href="../css/style.css">
  </head>
  <body>
    <?php include_once '../templates/header.php'; ?>
    <div class="content" id="content">
    <?php
      if(empty($result)) {
        $query = $_GET['search'];
        print "
        <div class=\"cards-header\" id=\"search-header\">
          <h3>NO RESULTS FOR \"$query\"</h3>
        <div class=\"dropdown\">";
        return;
      }
    ?>
    <form method="get">
    <div class="cards-header" id="search-header">
      <h3>SEARCH RESULTS FOR "<?php print $_GET['search']; ?>"</h3>
    <div class="dropdown">
      <label for="dropdown" class="dropdown-label">Sort</label>
      <select name="sort" id="sort-dropdown" class="form-group" onchange="this.form.submit()">
      		<option value="" selected="" disable="">Select Sort</option>
          <option id="ascending" value="Ascending"
          <?php
          if(isset($_GET['sort'])) {
            print $_GET['sort'] === 'Ascending' ? 'selected' : '';
          }
          ?>
          >Ascending</option>
          <option id="descending" value="Descending"
          <?php
          if(isset($_GET['sort'])) {
            print $_GET['sort'] === 'Descending' ? 'selected' : '';
          }
          ?>
          >Descending</option>
      </select>
    </div>
		<div class="dropdown">
      <label for="dropdown" class="dropdown-label">Order by</label>
      <select name="order" id="order-dropdown" class="form-group" onchange="this.form.submit()">
    		<option value="" selected="" disable="">Select Order by</option>
        <option id="title" value="title"
        <?php
        if(isset($_GET['order'])) {
          print $_GET['order'] === 'title' ? 'selected' : '';
        }?>
        >Title</option>
        <option id="start_date" value="airing_start"
        <?php
        if(isset($_GET['order'])) {
          print $_GET['order'] === 'airing_start' ? 'selected' : '';
        }?>
        >Airing start</option>
        <option id="score" value="score"
        <?php
        if(isset($_GET['order'])) {
          print $_GET['order'] === 'score' ? 'selected' : '';
        }?>
        >Score</option>
        <option id="type" value="type"
        <?php
        if(isset($_GET['order'])) {
          print $_GET['order'] === 'type' ? 'selected' : '';
        }?>
        >Type</option>
        <option id="episodes" value="episodes"
        <?php
        if(isset($_GET['order'])) {
          print $_GET['order'] === 'episodes' ? 'selected' : '';
        }?>
        >Episodes</option>
      </select>
    </div>
  </div>
  <input type="hidden" name="search" value="<?php print isset($_GET['search']) ? $_GET['search'] : ''; ?>">
  <input type="hidden" name="format" value="<?php print isset($_GET['format']) ? $_GET['format'] : ''; ?>">
  <input type="hidden" name="airing_status" value="<?php print isset($_GET['airing_status']) ? $_GET['airing_status'] : ''; ?>">
  <input type="hidden" name="genre" value="<?php print isset($_GET['genre']) ? $_GET['genre'] : ''; ?>">
</form>
    <div class="card-container" id="search-container">
    <?php
      foreach ($result as $row) {
          $id = htmlentities($row['id']);
          $title = htmlentities($row['title']);
          $imageId = htmlentities($row['image_id']);
          $viewAnimePath = URL_ROOT . "views/view_anime.php?id=$id";
          $viewImagePath = URL_ROOT . "views/view_image.php?image_id=$imageId";

          print "
          <div class=\"card\">
          <a href=\"$viewAnimePath\"><img class=\"card-img-container\" src=\"$viewImagePath\"></a>
            <p class=\"card-title\">$title</p>
          </div>";
      }
    ?>
  </div>
    </div>
  </body>
</html>
