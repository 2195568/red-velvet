<?php
include_once '../../resources/bootstrap.php';
$path = URL_ROOT . 'images/pixeltrue-error.png';
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Page Not Found</title>
    <link rel="stylesheet" href="<?php print URL_ROOT . 'css/style.css'; ?>">
  </head>
  <body>
    <div class="error-page">
      <img src="<?php print $path ?>" height="500">
      <a class="normal-button"type="button" href="<?php print URL_ROOT; ?>"
        >Return Home</a>
    </div>
  </body>
</html>
