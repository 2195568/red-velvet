<header class="header">
<form action="<?php print URL_ROOT . 'views/search.php'; ?>" method="get">
<div class="header-top">
	<div class="home-area">
		<p class="home-label"><a class="home-label__content" href="<?php print URL_ROOT; ?>" title="Otacon">
      <img src="../images/otacon_logo.png" alt="" height="50">
    </a></p>
	</div>
	<div class="form-container">
		<input class="form-control" id="search-input" placeholder="Enter keywords..."
    name="search" type="text" autocomplete="off"required>
		<button type="submit" class="form-button" id="search-button">SEARCH</button>
	</div>
	<nav>
	<ul class="nav">
		<li class="nav__item">
			<a href="#"  title="Anime">
				<i id="anime-nav" class="fa fa-play" aria-hidden="true">
					<span class="nav-item-title" id="anime-nav">Anime</span>
				</i>
			</a>
		</li>
		<li class="nav__item"><a href="#" title="Manga"><i class="fa fa-newspaper-o" id="manga-nav" aria-hidden="true"><span class="nav-item-title" id="manga-nav">Manga</span></i></a></li>
	</ul>
	</nav>
</div>
<div class="header-bot" id="header-bot">

			<div class="dropdown">
				<label for="dropdown" class="dropdown-label">TYPE</label>
				<select name="format" id="format-dropdown" class="form-group">
					<option value="" selected="" disable="">Select Type</option>
					<option id="TV" value="TV"
					<?php
					if(isset($_GET['format'])) {
						print $_GET['format'] === 'TV' ? 'selected' : '';
					}?>
					>TV</option>
					<option id="OVA" value="OVA"
					<?php
					if(isset($_GET['format'])) {
						print $_GET['format'] === 'OVA' ? 'selected' : '';
					}?>
					>OVA</option>
					<option id="Movie" value="Movie"
					<?php
					if(isset($_GET['format'])) {
						print $_GET['format'] === 'Movie' ? 'selected' : '';
					}?>
					>Movie</option>
					<option id="Special" value="Special"
					<?php
					if(isset($_GET['format'])) {
						print $_GET['format'] === 'Special' ? 'selected' : '';
					}?>
					>Special</option>
					<option id="ONA" value="ONA"
					<?php
					if(isset($_GET['format'])) {
						print $_GET['format'] === 'ONA' ? 'selected' : '';
					}?>
					>ONA</option>
					<option id="Music" value="Music"
					<?php
					if(isset($_GET['format'])) {
						print $_GET['format'] === 'Music' ? 'selected' : '';
					}?>>Music</option>
				</select>
				</div>
				<div class="dropdown">
					<label for="dropdown" class="dropdown-label">Airing Status</label>
						<select name="airing_status" id="status-dropdown" class="form-group">
							<option value="" selected="" disable="">Select Airing Status</option>
							<option id="Airing" value="Airing"
							<?php
							if(isset($_GET['airing_status'])) {
								print $_GET['airing_status'] === 'Airing' ? 'selected' : '';
							}?>
							>Airing</option>
							<option id="Completed" value="Completed"
							<?php
							if(isset($_GET['airing_status'])) {
								print $_GET['airing_status'] === 'Completed' ? 'selected' : '';
							}?>
							>Completed</option>
							<option id="Upcoming" value="Upcoming"
							<?php
							if(isset($_GET['airing_status'])) {
								print $_GET['airing_status'] === 'Upcoming' ? 'selected' : '';
							}?>
							>Upcoming</option>
						</select>
					</div>
					<div class="dropdown">
						<label for="dropdown" class="dropdown-label">Genres</label>
						<select name="genre" id="genre-dropdown" class="form-group">
							<option value="" selected="" disable="">Select Genre</option>
							<option value="Action" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Action' ? 'selected' : '';
							}?>>Action</option>
							<option value="Adventure" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Adventure' ? 'selected' : '';
							}?>>Adventure</option>
							<option value="Cars" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Cars' ? 'selected' : '';
							}?>>Cars</option>
							<option value="Comedy" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Comedy' ? 'selected' : '';
							}?>>Comedy</option>
							<option value="Dementia" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Dementia' ? 'selected' : '';
							}?>>Dementia</option>
							<option value="Demons" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Demons' ? 'selected' : '';
							}?>>Demons</option>
							<option value="Mystery" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Mystery' ? 'selected' : '';
							}?>>Mystery</option>
							<option value="Drama" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Drama' ? 'selected' : '';
							}?>>Drama</option>
							<option value="Ecchi" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Ecchi' ? 'selected' : '';
							}?>>Ecchi</option>
							<option value="Fantasy" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Fantasy' ? 'selected' : '';
							}?>>Fantasy</option>
							<option value="Game" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Game' ? 'selected' : '';
							}?>>Game</option>
							<option value="Hentai" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Hentai' ? 'selected' : '';
							}?>>Hentai</option>
							<option value="Historical" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Historical' ? 'selected' : '';
							}?>>Historical</option>
							<option value="Horror" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Horror' ? 'selected' : '';
							}?>>Horror</option>
							<option value="Kvalues" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Kvalues' ? 'selected' : '';
							}?>>Kvalues</option>
							<option value="Magic" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Magic' ? 'selected' : '';
							}?>>Magic</option>
							<option value="Martial Arts" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Martial Arts' ? 'selected' : '';
							}?>>Martial Arts</option>
							<option value="Mecha" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Mecha' ? 'selected' : '';
							}?>>Mecha</option>
							<option value="Music" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Music' ? 'selected' : '';
							}?>>Music</option>
							<option value="Parody" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Parody' ? 'selected' : '';
							}?>>Parody</option>
							<option value="Samurai" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Samurai' ? 'selected' : '';
							}?>>Samurai</option>
							<option value="Romance" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Romance' ? 'selected' : '';
							}?>>Romance</option>
							<option value="School" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'School' ? 'selected' : '';
							}?>>School</option>
							<option value="Sci Fi" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Sci Fi' ? 'selected' : '';
							}?>>Sci Fi</option>
							<option value="Shoujo" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Shoujo' ? 'selected' : '';
							}?>>Shoujo</option>
							<option value="Shoujo Ai" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Shoujo Ai' ? 'selected' : '';
							}?>>Shoujo Ai</option>
							<option value="Shounen" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Shounen' ? 'selected' : '';
							}?>>Shounen</option>
							<option value="Shounen Ai" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Shounen Ai' ? 'selected' : '';
							}?>>Shounen Ai</option>
							<option value="Space" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Space' ? 'selected' : '';
							}?>>Space</option>
							<option value="Sports" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Sports' ? 'selected' : '';
							}?>>Sports</option>
							<option value="Super Power" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Super Power' ? 'selected' : '';
							}?>>Super Power</option>
							<option value="Vampire" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Vampire' ? 'selected' : '';
							}?>>Vampire</option>
							<option value="Yaoi" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Yaoi' ? 'selected' : '';
							}?>>Yaoi</option>
							<option value="Yuri" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Yuri' ? 'selected' : '';
							}?>>Yuri</option>
							<option value="Harem" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Harem' ? 'selected' : '';
							}?>>Harem</option>
							<option value="Slice Of Life" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Slice Of Life' ? 'selected' : '';
							}?>>Slice Of Life</option>
							<option value="Supernatural" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Supernatural' ? 'selected' : '';
							}?>>Supernatural</option>
							<option value="Military" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Military' ? 'selected' : '';
							}?>>Military</option>
							<option value="Police" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Police' ? 'selected' : '';
							}?>>Police</option>
							<option value="Psychological" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Psychological' ? 'selected' : '';
							}?>>Psychological</option>
							<option value="Thriller" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Thriller' ? 'selected' : '';
							}?>>Thriller</option>
							<option value="Seinen" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Seinen' ? 'selected' : '';
							}?>>Seinen</option>
							<option value="Josei" <?php
							if(isset($_GET['genre'])) {
								print $_GET['genre'] === 'Josei' ? 'selected' : '';
							}?>>Josei</option>
						</select>
					</div>
	</div>
</form>
</header>
