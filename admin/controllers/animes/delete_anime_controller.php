<?php
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'Anime.php';

  validateSession();

  $anime = new Anime();
  $row = $anime->getAnimeById($_GET['id']);

  // Verify if user_id exists
  if ($row !== false) {
      $_SESSION['id'] = htmlentities($row['id']);
      $_SESSION['title'] = htmlentities($row['title']);
  } else {
      $_SESSION['error_notification'] = 'Anime not found';
      redirect('manage_animes.php');
  }

  // if form has been submitted, verify values
  if(hasPostElement('id')) {
    $anime->deleteAnime($_POST['id']);
    $_SESSION['success_notification'] = 'Record deleted';
    redirect('manage_animes.php');
  }
