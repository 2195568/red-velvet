<?php // TODO: Fix Youtube video url, get only the video id
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'Anime.php';

  validateSession();

  if (isset($_POST['submit'])) {
      verifyForm();
  }

  $anime = new Anime();
  $row = $anime->getAnimeById($_GET['id']);

  if ($row !== false) {
      $_SESSION['id'] = htmlentities($row['id']);
      $_SESSION['title'] = htmlentities($row['title']);
      $_SESSION['type'] = htmlentities($row['type']);
      $_SESSION['synopsis'] = htmlentities($row['synopsis']);
      $_SESSION['genres'] = explode(',', htmlentities($row['genres']));
      $_SESSION['producers'] = explode(',', htmlentities($row['producers']));
      $_SESSION['score'] = htmlentities($row['score']);
      $_SESSION['airing_start'] = htmlentities($row['airing_start']);
      $_SESSION['episodes'] = htmlentities($row['episodes']);
      $_SESSION['image_id'] = htmlentities($row['image_id']);
      $_SESSION['url'] = htmlentities($row['url']);
      $_SESSION['trailer_url'] = htmlentities($row['trailer_url']);
  } else {
      $_SESSION['error_notification'] = 'Anime ID not found';
      redirect('manage_animes.php');
  }

  function verifyForm()
  {
      if (!hasPostElement('title')) {
          $_SESSION['error_notification']  = 'Title is required';
          redirect('edit_anime.php?id=' . $_POST['id']);
      } elseif (!hasPostElement('type')) {
          $_SESSION['error_notification']  = 'Anime type is required';
          redirect('edit_anime.php?id=' . $_POST['id']);
      } elseif (!hasPostElement('airing_start')) {
          $_SESSION['error_notification']  = 'The airing date is required';
          redirect('edit_anime.php?id=' . $_POST['id']);
      } elseif (count($_FILES) < 0) {
          $_SESSION['error_notification']  = 'Image is required';
          redirect('edit_anime.php?id=' . $_POST['id']);
      } elseif (!hasPostElement('synopsis')) {
          $_SESSION['error_notification']  = 'Synopsis is required';
          redirect('edit_anime.php?id=' . $_POST['id']);
      }

      verifyDuplication();
  }

  function verifyDuplication()
  {
      print_r($_POST);
      $anime = new Anime();

      $imageId = $_POST['image_id'];

      // Verify image
      if (is_uploaded_file($_FILES['image']['tmp_name'])) {
          $imageProperties = getimageSize($_FILES['image']['tmp_name']);
          $imageType = $_FILES["image"]["type"];
          $imageData = file_get_contents($_FILES["image"]["tmp_name"]);

          $data = array(
                  'type' => $imageType,
                  'data' => file_get_contents($_FILES['image']['tmp_name'])
                  );

          $anime->addImage($data);

          // Update Anime's image id
          $row = $anime->getLastImageId();
          $imageId = $row['id'];
      }



      $id = $_POST['id'];
      $title = $_POST['title'];
      $type = $_POST['type'];
      $synopsis = $_POST['synopsis'];
      $genres = implode(", ", $_POST['genres']);
      $producers = $_POST['producers'];
      $score = $_POST['score'];
      $airing_start = $_POST['airing_start'] === '' ? null : $_POST['airing_start'];
      $episodes = $_POST['episodes'];
      $image_id = $imageId;
      $url = $_POST['url'];
      $trailer_url = hasPostElement($_POST['trailer_url']) ? $_POST['trailer_url'] : null;

      print "<pre>";
      print  $trailer_url;
      print "</pre>";

      $row = $anime->getAnimeById($_POST['id']);

      // If there is no changes, redirect to manage_animes.php
      if ($row !== false) {
          if ($row['title'] === $title && $row['type'] === $type && $row['synopsis'] === $synopsis
        && $row['genres'] === $genres && $row['producers'] === $producers && $row['score'] === $score && $row['airing_start'] === $airing_start && $row['episodes'] === $episodes && $row['image_id'] === $image_id && $row['url'] === $url) {
              redirect('manage_animes.php');
          }
      }

      $data = array(
                'title' => $title,
                'type' => $type,
                'synopsis' => $synopsis,
                'genres' => $genres,
                'producers' => $producers,
                'score' => $score,
                'airing_start' => $airing_start,
                'episodes' => $episodes,
                'image_id' => $image_id,
                'url' => $url,
                'trailer_url' => $trailer_url
            );

      $anime->updateAnime($id, $data);


      $_SESSION['success_notification'] = 'Anime "' . $_POST['title'] . '" has been successfully updated!';
      redirect('manage_animes.php');
  }

  function findString($string, $array)
  {
    foreach ($array as $value) {
      if($string === $value) {
        return true;
      }
    }
    return false;
  }
