<?php // TODO: Fix the edit view to support multiple producer etc.
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'Anime.php';

  validateSession();

  if (isset($_POST['submit'])) {
      verifyForm();
  }

  function verifyForm()
  {
      if (!hasPostElement('title')) {
          $_SESSION['error_notification']  = 'Title is required';
          redirect('add_anime.php');
      } elseif (!hasPostElement('type')) {
          $_SESSION['error_notification']  = 'Anime type is required';
          redirect('add_anime.php');
      } elseif (!hasPostElement('airing_start')) {
          $_SESSION['error_notification']  = 'The airing date is required';
          redirect('add_anime.php');
      } elseif (count($_FILES) < 0) {
          $_SESSION['error_notification']  = 'Image is required';
          redirect('add_anime.php');
      } elseif (!hasPostElement('synopsis')) {
          $_SESSION['error_notification']  = 'Synopsis is required';
          redirect('add_anime.php');
      }

      verifyDuplication();
  }

  function verifyDuplication()
  {
      $anime = new Anime();
      $row = $anime->getAnimeByTitle($_POST['title']);

      if ($row !== false) {
          $hasDuplicate = $row['title'] === $_POST['title'];

          if ($hasDuplicate) {
              $_SESSION['error_notification'] = 'Anime is already taken';
              redirect('add_anime.php');
          }
      } else {
          addImage();
          addAnime();

          $_SESSION['success_notification'] = 'Anime "' . $_POST['title'] . '" has been successfully added!';
          redirect('manage_animes.php');
      }
  }

  function addImage()
  {
      $anime = new Anime();
      $row = $anime->getAnimeByTitle($_POST['title']);

      $imageProperties = getimageSize($_FILES['image']['tmp_name']);
      $imageType = $_FILES["image"]["type"];
      $imageData = file_get_contents($_FILES["image"]["tmp_name"]);

      $data = array(
        'type' => $imageType,
        'data' => file_get_contents($_FILES['image']['tmp_name'])
      );

      $anime->addImage($data);
  }

  function addAnime()
  {
      $anime = new Anime();
      // Get last image id
      $row = $anime->getLastImageId();
      $lastImageId = $row['id'];

      $animeData = array(
      'title' => $_POST['title'],
      'type' => $_POST['type'],
      'synopsis' => $_POST['synopsis'],
      'genres' => implode(', ', $_POST['genres']),
      'producers' => implode(', ', $_POST['producers']),
      'score' => hasPostElement($_POST['score']) ? $_POST['score'] : 0,
      'airing_start' => $_POST['airing_start'] === '' ? null : $_POST['airing_start'],
      'episodes' => hasPostElement($_POST['score']) ? $_POST['score'] : 0,
      'image_id' => $lastImageId,
      'url' => $_POST['url'],
      'trailer_url' => isset($_POST['trailer_url']) ? $_POST['trailer_url'] : null
    );

      // Add anime data
      $anime->addAnime($animeData);
  }
