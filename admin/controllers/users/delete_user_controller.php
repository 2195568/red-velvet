<?php
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'User.php';

  validateSession();

  $user = new User();
  $row = $user->getUserById($_GET['id']);

  // Verify if user_id exists
  if ($row !== false) {
      $_SESSION['id'] = htmlentities($row['id']);
      $_SESSION['username'] = htmlentities($row['username']);
  } else {
      $_SESSION['error_notification'] = 'User ID not found';
      redirect('manage_users.php');
  }

  // if form has been submitted, verify values
  if(hasPostElement('id')) {
    $user->deleteUser($_POST['id']);
    $_SESSION['success_notification'] = 'Record deleted';
    redirect('manage_users.php');
  }
