<?php
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'User.php';

  validateSession();

  // Verify form values upon submission
  if (isset($_POST['submit'])) {
      verifyForm();
  }

    // Verify form values
  function verifyForm()
  {
      if (!hasPostElement('username')) {
          $_SESSION['error_notification']  = 'Username and password are required';
          redirect('edit_user.php?id=' . $_POST['id']);
      }
      
      if (!hasPostElement('password')){
        $_SESSION['error_notification']  = 'Password is required';
        redirect('edit_user.php?id=' . $_POST['id']);
      }

      verifyDuplication();
  }

  function verifyDuplication()
  {
      $user = new User();
      $row = $user->getUserByName($_POST['username']);

      if ($row !== false) {
          $_SESSION['error_notification'] = 'Username is already taken';
          redirect('edit_user.php?id=' . $_GET['id']);
      } else {
          $data = array(
              'username' => $_POST['username'],
              'password' => $_POST['password']
          );

          $user->updateUser($_POST['id'], $data);
          $_SESSION['success_notification'] = 'User "' . $_POST['username'] . '" has been successfully updated!';
          redirect('manage_users.php');
      }
  }

  $user = new User();
  $row = $user->getUserById($_GET['id']);

  if ($row !== false) {
      $_SESSION['id'] = htmlentities($row['id']);
      $_SESSION['username'] = htmlentities($row['username']);
      $_SESSION['password'] = htmlentities($row['password']);
  } else {
      $_SESSION['error_notification'] = 'User ID not found';
      redirect('manage_users.php');
  }
