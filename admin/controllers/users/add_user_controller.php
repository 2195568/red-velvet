<?php
    require_once '../../../resources/bootstrap.php';
    require_once MODELS_PATH . 'User.php';

    validateSession();

    if (isset($_POST['submit'])) {
        verifyForm();
    }

    function verifyForm()
    {
        if (!hasPostElement('username') || !hasPostElement('password')) {
            $_SESSION['error_notification']  = 'Username and password are required';
            redirect('add_user.php');
        }

        verifyDuplication();
    }

    function verifyDuplication()
    {
        $user = new User();
        $row = $user->getUserByName($_POST['username']);

        if ($row !== false) {
            $_SESSION['error_notification'] = 'Username is already taken';
            redirect('add_user.php');
        } else {
            $data = array(
                'username' => $_POST['username'],
                'password' => $_POST['password']
            );
            $user->addUser($data);

            $_SESSION['success_notification'] = 'New user "' . $_POST['username'] . '" has been successfully added!';
            redirect('manage_users.php');
        }
    }
