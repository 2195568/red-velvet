<?php
	require_once '../resources/bootstrap.php';
	require_once MODELS_PATH . 'User.php';


	if(isset($_POST['username']) || isset($_POST['password'])) {
		validateLoginForm();
	}

	function validateLoginForm() {
		if(!hasPostElement('username') || !hasPostElement('password')) {
			$_SESSION['error_notification']  = 'Username and password are required';
			redirect('index.php');
		}

		if(isPasswordCorrect()) {
			// Login success
			$username = $_POST['username'];
			$password = $_POST['password'];

			logIn($username, $password);
			redirect('../views/home.php');
		} else {
			$_SESSION['error_notification']  = 'Incorrect password';
			redirect('index.php');
		}
	}

	function isPasswordCorrect()
	{// TODO: Fix db
		$user = new User();
		$row = $user->getUserByName($_POST['username']);

		if($row !== false) {
			if($row['password'] === $_POST['password']) {
				return true;
			}
		}
		return false;
	}
