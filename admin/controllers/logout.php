<?php
require_once '../../resources/bootstrap.php';

unset($_SESSION['username'], $_SESSION['password']);
session_destroy();
redirect('../index.php');
