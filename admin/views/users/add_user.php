<?php require_once '../../controllers/users/add_user_controller.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>Add New User</title>
		<link rel="stylesheet" href="../../css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
	<body>
		<div class="container">
			<form method="post">
				<h2>Create New User</h2>
				<?php require_once '../flash_message.php'; ?>
				<div class="field">
					<label>Username:</label>
					<input type="text" title="username" name="username">
				</div>
				<div class="field">
					<label>Password:</label>
					<input type="text" title="password" name="password">
				</div>
				<div align="center">
					<button type="submit" name="submit" value="Add">Add</button>
					<a href="manage_users.php">Cancel</a>
				</div>
			</form>
		</div>
	</body>
</html>
