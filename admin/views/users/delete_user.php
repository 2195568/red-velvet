<?php require_once '../../controllers/users/delete_user_controller.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Delete User</title>
    <link rel="stylesheet" href="../../css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class="container">
    	<form method="post">
          <h2>Delete User "<?php print $_SESSION['username']; ?>" ?</h2>
          <?php require_once '../flash_message.php'; ?>
        <div align="center">
          <input type="hidden" name="id" value="<?php print $_SESSION['id'];?>">
    		  <button type="submit">Delete</button>
          <a href="manage_users.php">Cancel</a>
        </div>
    	</form>
    </div>
  </body>
</html>
