<?php require_once '../../controllers/home_controller.php'?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>Manage Users</title>
		<link rel="stylesheet" href="../../css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script type="text/javascript" src="../../js/index.js" defer>
		</script>
	</head>
	<body>
		<?php include_once '../../templates/header.php' ?>
		<?php require_once '../flash_message.php'; ?>
		<?php require_once 'users_table.php'; ?>
	</body>
</html>
