<?php
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'User.php';
?>
<div class="table-container">
  <div class="add-item-container">
    <a href="./add_user.php"><span class="add-button">Add new user</span></a>
  </div>
<table>
  <tr>
    <th>Username</th>
    <th>Password</th>
    <th>Edit</th>
    <th>Delete</th>
  </tr>
  <?php
    $user = new User();
    $users = $user->getAll();

    foreach ($users as $row) {
        $id = $row['id'];
        $username = $row['username'];
        $password = $row['password'];
        $edit = "<a href=\"edit_user.php?id=$id\">Edit</a>";
        $delete ="<a href=\"delete_user.php?id=$id\">Delete</a>";

        print "
        <tr>
          <td>$username</td>
          <td>$password</td>
          <td>$edit</td>
          <td>$delete</td>
        </tr>
      ";
    }

  ?>
</table>
</div>
