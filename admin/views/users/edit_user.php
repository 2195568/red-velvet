<?php require_once '../../controllers/users/edit_user_controller.php';?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Edit User</title>
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class="container">
      <form method="post">
        <h2>Edit User</h2>
        <?php require_once '../flash_message.php'; ?>
        <div class="field">
          <label>Username:</label>
          <input type="text" name="username" value="<?php print $_SESSION['username'];?>" autocomplete="off">
        </div>
        <div class="field">
          <label>Password:</label>
          <input type="text" name="password" value="<?php print $_SESSION['password'];?>" autocomplete="off">
        </div>
        <div class="center">
          <input type="hidden" name="id" value="<?php print $_SESSION['id'];?>" autocomplete="off">
          <button type="submit" name="submit" value="Submit">Submit</button>
          <a href="manage_users.php">Cancel</a>
        </div>
      </form>
    </div>
    <?php unset($_SESSION['username'], $_SESSION['password'], $_SESSION['id']);?>
  </body>
</html>
