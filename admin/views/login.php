<?php require_once getcwd() . '/controllers/login_controller.php'; ?>
<div class="container">
	<form method="post">
    <h2>Log in to your account</h2>
    <?php require_once './views/flash_message.php'; ?>
		<div class="field" tabindex="1">
			<label for="username">Username</label>
			<input name="username" type="text" title="username" autocomplete="off" required>
		</div>

    <div class="field" tabindex="2">
			<label for="password">Password</label>
			<input name="password" type="password" title="password" required>
		</div>

		<button type="submit">Log in</button>
	</form>
</div>
