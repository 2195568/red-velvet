<?php
  // Flash messages for sessions/database process notifications
  if(isset($_SESSION['error_notification'])) {
    print "
    <div class=\"flash fail\">
    <span>" . $_SESSION['error_notification'] . "</span>
    </div>";
    unset($_SESSION['error_notification']);
  }

  if(isset($_SESSION['success_notification'])) {
    print "
    <div class=\"flash success\">
    <span>" . $_SESSION['success_notification'] . "
    </span>
    </div>";
    unset($_SESSION['success_notification']);
  }
