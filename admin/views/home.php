<?php require_once '../../resources/bootstrap.php' ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Otacon | Admin</title>
    <link rel="stylesheet" href="../css/style.css">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="../js/index.js" defer>
    </script>
  </head>
  <body>
    <?php include_once '../templates/header.php' ?>
  </body>
  <script type="text/javascript">
    var manageAnimeNav = document.getElementById('manage-animes');
    manageAnimeNav.click();
  </script>
</html>
