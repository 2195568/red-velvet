<?php require_once '../../controllers/animes/delete_anime_controller.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Delete Anime</title>
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class="container">
      <form method="post">
          <h2>Delete Anime "<?php print $_SESSION['title']; ?>" ?</h2>
          <?php require_once '../flash_message.php'; ?>
        <div align="center">
          <input type="hidden" name="id" value="<?php print $_SESSION['id'];?>">
          <button type="submit">Delete</button>
          <a href="manage_animes.php">Cancel</a>
        </div>
      </form>
    </div>
  </body>
</html>

</head>
<body>
