<?php require_once '../../../resources/bootstrap.php';?>
<?php require_once MODELS_PATH . 'Anime.php';?>

<div class="table-container">
  <div class="add-item-container">
    <a href="add_anime.php"><span class="add-button">Add new anime</span></a>
  </div>
<table>
  <tr>
    <th>Title</th>
    <th>View</th>
    <th>Edit</th>
    <th>Delete</th>
  </tr>
  <?php
    $anime = new Anime();
    $animes = $anime->getAll();

    foreach ($animes as $row) {
        $id = htmlentities($row['id']);
        $title = htmlentities($row['title']);
        $viewPath = "<a href=\"view_anime.php?id=$id\">View</a>";
        $edit = "<a href=\"edit_anime.php?id=$id\">Edit</a>";
        $delete ="<a href=\"delete_anime.php?id=$id\">Delete</a>";

        print "
        <tr>
          <td>$title</td>
          <td>$viewPath</td>
          <td>$edit</td>
          <td>$delete</td>
        </tr>";
    }
  ?>
</table>
</div>
