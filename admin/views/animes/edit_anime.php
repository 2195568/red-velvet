<?php require_once '../../../resources/bootstrap.php'; ?>
<?php  require_once '../../controllers/animes/edit_anime_controller.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Edit Anime</title>
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class="container">
    <form method="post" enctype="multipart/form-data">
      <h2>Edit Anime "<?php print $_SESSION['title']; ?>"</h2>
      <?php require_once '../flash_message.php'; ?>

      <div class="field">
        <label>
          Title:
        </label>
        <input type="text" title="title" name="title" value="<?php print $_SESSION['title']; ?>" autocomplete="off" >
      </div>

      <div class="field">
          <label>Genres</label><br>
          <div class="scrollbox">
            <label>Action
              <input
                type="checkbox"
                name="genres[]"
                value="Action"
                <?php echo preg_match('/\bAction\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Adventure
              <input
                type="checkbox"
                name="genres[]"
                value="Adventure"
                <?php echo preg_match('/\bAdventure\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Cars
              <input
                type="checkbox"
                name="genres[]"
                value="Cars"
                <?php echo preg_match('/\bCars\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Comedy
              <input
                type="checkbox"
                name="genres[]"
                value="Comedy"
                <?php echo preg_match('/\bComedy\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Dementia
              <input
                type="checkbox"
                name="genres[]"
                value="Dementia"
                <?php echo preg_match('/\bDementia\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Demons
              <input
                type="checkbox"
                name="genres[]"
                value="Demons"
                <?php echo preg_match('/\bDemons\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Mystery
              <input
                type="checkbox"
                name="genres[]"
                value="Mystery"
                <?php echo preg_match('/\bMystery\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Drama
              <input
                type="checkbox"
                name="genres[]"
                value="Drama"
                <?php echo preg_match('/\bDrama\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Ecchi
              <input
                type="checkbox"
                name="genres[]"
                value="Ecchi"
                <?php echo preg_match('/\bEcchi\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Fantasy
              <input
                type="checkbox"
                name="genres[]"
                value="Fantasy"
                <?php echo preg_match('/\bFantasy\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Game
              <input
                type="checkbox"
                name="genres[]"
                value="Game"
                <?php echo preg_match('/\bGame\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Hentai
              <input
                type="checkbox"
                name="genres[]"
                value="Hentai"
                <?php echo preg_match('/\bHentai\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Historical
              <input
                type="checkbox"
                name="genres[]"
                value="Historical"
                <?php echo preg_match('/\bHistorical\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Horror
              <input
                type="checkbox"
                name="genres[]"
                value="Horror"
                <?php echo preg_match('/\bHorror\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Kvalues
              <input
                type="checkbox"
                name="genres[]"
                value="Kvalues"
                <?php echo preg_match('/\bKvalues\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Magic
              <input
                type="checkbox"
                name="genres[]"
                value="Magic"
                <?php echo preg_match('/\bMagic\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Martial Arts
              <input
                type="checkbox"
                name="genres[]"
                value="Martial Arts"
                <?php echo preg_match('/\bMartial Arts\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Mecha
              <input
                type="checkbox"
                name="genres[]"
                value="Mecha"
                <?php echo preg_match('/\bMecha\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Music
              <input
                type="checkbox"
                name="genres[]"
                value="Music"
                <?php echo preg_match('/\bMusic\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Parody
              <input
                type="checkbox"
                name="genres[]"
                value="Parody"
                <?php echo preg_match('/\bParody\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Samurai
              <input
                type="checkbox"
                name="genres[]"
                value="Samurai"
                <?php echo preg_match('/\bSamurai\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Romance
              <input
                type="checkbox"
                name="genres[]"
                value="Romance"
                <?php echo preg_match('/\bRomance\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>School
              <input
                type="checkbox"
                name="genres[]"
                value="School"
                <?php echo preg_match('/\bSchool\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Sci Fi
              <input
                type="checkbox"
                name="genres[]"
                value="Sci Fi"
                <?php echo preg_match('/\bSci Fi\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Shoujo
              <input
                type="checkbox"
                name="genres[]"
                value="Shoujo"
                <?php echo preg_match('/\bShoujo\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Shoujo Ai
              <input
                type="checkbox"
                name="genres[]"
                value="Shoujo Ai"
                <?php echo preg_match('/\bShoujo Ai\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Shounen
              <input
                type="checkbox"
                name="genres[]"
                value="Shounen"
                <?php echo preg_match('/\bShounen\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Shounen Ai
              <input
                type="checkbox"
                name="genres[]"
                value="Shounen Ai"
                <?php echo preg_match('/\bShounen Ai\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Space
              <input
                type="checkbox"
                name="genres[]"
                value="Space"
                <?php echo preg_match('/\bSpace\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Sports
              <input
                type="checkbox"
                name="genres[]"
                value="Sports"
                <?php echo preg_match('/\bSports\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Super Power
              <input
                type="checkbox"
                name="genres[]"
                value="Super Power"
                <?php echo preg_match('/\bSuper Power\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Vampire
              <input
                type="checkbox"
                name="genres[]"
                value="Vampire"
                <?php echo preg_match('/\bVampire\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Yaoi
              <input
                type="checkbox"
                name="genres[]"
                value="Yaoi"
                <?php echo preg_match('/\bYaoi\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Yuri
              <input
                type="checkbox"
                name="genres[]"
                value="Yuri"
                <?php echo preg_match('/\bYuri\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Harem
              <input
                type="checkbox"
                name="genres[]"
                value="Harem"
                <?php echo preg_match('/\bHarem\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Slice Of Life
              <input
                type="checkbox"
                name="genres[]"
                value="Slice Of Life"
                <?php echo preg_match('/\bSlice Of Life\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Supernatural
              <input
                type="checkbox"
                name="genres[]"
                value="Supernatural"
                <?php echo preg_match('/\bSupernatural\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Military
              <input
                type="checkbox"
                name="genres[]"
                value="Military"
                <?php echo preg_match('/\bMilitary\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Police
              <input
                type="checkbox"
                name="genres[]"
                value="Police"
                <?php echo preg_match('/\bPolice\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Psychological
              <input
                type="checkbox"
                name="genres[]"
                value="Psychological"
                <?php echo preg_match('/\bPsychological\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Thriller
              <input
                type="checkbox"
                name="genres[]"
                value="Thriller"
                <?php echo preg_match('/\bThriller\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Seinen
              <input
                type="checkbox"
                name="genres[]"
                value="Seinen"
                <?php echo preg_match('/\bSeinen\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
            <label>Josei
              <input
                type="checkbox"
                name="genres[]"
                value="Josei"
                <?php echo preg_match('/\bJosei\b/', implode(',', $_SESSION['genres'])) ? 'checked' : ''; ?>
                ></label>
          </div>
      </div>

      <div class="field">
        <label>Type:</label>
        <select name="type" id="type-select">
            <option disabled selected>Select type</option>
            <option value="Movie" <?php print $_SESSION['type'] === 'Movie' ? 'selected' : ''; ?> >Movie</option>
            <option value="TV" <?php print $_SESSION['type'] === 'TV' ? 'selected' : ''; ?> >TV</option>
            <option value="OVA" <?php print $_SESSION['type'] === 'OVA' ? 'selected' : ''; ?> >OVA</option>
            <option value="Special" <?php print $_SESSION['type'] === 'Special' ? 'selected' : ''; ?>>Special</option>
            <option value="ONA" <?php print $_SESSION['type'] === 'ONA' ? 'selected' : ''; ?>>ONA</option>
            <option value="Music" <?php print $_SESSION['type'] === 'Music' ? 'selected' : ''; ?>>Music</option>
        </select>
      </div>

      <div class="field">
        <label>Trailer URL (Optional):</label>
        <input type="url" id="type-select" name="trailer_url" value="<?php print $_SESSION['trailer_url']; ?>">
      </div>
      <div class="field">
        <label>
          Airing start:
        </label>
        <input type="date" title="airing start" name="airing_start" value="<?php print $_SESSION['airing_start']; ?>" autocomplete="off">
      </div>

      <div class="field">
        <label id="producer-input">
          Producers:
        </label>
        <input type="text" name="producers" value="<?php print implode(',', $_SESSION['producers']); ?>" autocomplete=\"off\">
      </div>

      <div class="field">
        <label>
          Score (Optional):
        </label>
        <input type="number" name="score" step=".01" value="<?php print $_SESSION['score']; ?>" autocomplete="off">
      </div>

      <div class="field">
        <label>Episodes (Optional):</label>
        <input type="number" name="episodes" value="<?php print $_SESSION['episodes']; ?>" autocomplete="off">
      </div>

      <div class="field">
        <label>URL:</label>
        <input type="url" name="url" value="<?php print $_SESSION['url']; ?>" autocomplete="off">
      </div>

      <div class="field">
        <label for="synopsis">Synopsis:</label>
        <textarea name="synopsis" rows="8" cols="80" required><?php print $_SESSION['synopsis']; ?></textarea>
      </div>

      <div class="field">
        <img src="view_image.php?image_id=<?php print $_SESSION['image_id']; ?>"><br>
        <label>Image:</label>
        <input type="file" id="image" title="image" name="image" accept="image/*" >
        </br>
        </br>
      </div>

      <div align="center">
        <input type="hidden" name="id" value="<?php print $_SESSION['id'] ?>">
        <input type="hidden" name="image_id" value="<?php print $_SESSION['image_id'] ?>">
        <button type="submit" name="submit" value="Submit">Submit</button>
        <a href="manage_animes.php">Back</a>
      </div>
    </form>
  </div>
    <?php
      unset($_SESSION['title']);
      unset($_SESSION['$id']);
      unset($_SESSION['type']);
      unset($_SESSION['synopsis']);
      unset($_SESSION['genres']);
      unset($_SESSION['producers']);
      unset($_SESSION['score']);
      unset($_SESSION['airing_start']);
      unset($_SESSION['episodes']);
      unset($_SESSION['image_id']);
      unset($_SESSION['url']);
      unset($_SESSION['trailer_url']);
    ?>
  </body>
</html>
