<?php
include_once '../../../resources/bootstrap.php';
include_once MODELS_PATH . 'Anime.php';

if(!isset($_GET['id'])) {
  redirect('error_page.php');
} else {
  $anime = new Anime();
  $row = $anime->getAnimeById($_GET['id']);

  if($row !== false) {
    $id = htmlentities($row['id']);
    $type = htmlentities($row['type']);
    $title = htmlentities($row['title']);
    $synopsis = htmlentities($row['synopsis']);
    $producers = htmlentities($row['producers']);
    $score = htmlentities($row['score']);
    $airing_start = htmlentities($row['airing_start']);
    $episodes = htmlentities($row['episodes']);
    $imageId = htmlentities($row['image_id']);
    $url = htmlentities($row['url']);
    $trailer_url = htmlentities($row['trailer_url']);
    $trailer_url = explode('/', $trailer_url);
    $trailer_id = end($trailer_url);
    $viewImagePath = URL_ROOT . "views/animes/view_image.php?image_id=$imageId";
    $genres = explode(',', htmlentities($row['genres']));
    $genres = implode(', ', $genres);
  }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>View Anime</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link rel="stylesheet" href="<?php print URL_ROOT . 'css/style.css' ?>">
  </head>
  <body>
    <div class="anime-wrapper">
    <div class="back-container">
      <div class="add-item-container">
        <a href="javascript:history.back()"><span class="add-button">Back</span></a>
      </div>
    </div>
    <div class="anime-view-container">
      <div class="anime-vertical-container">
        <div class="">
          <img src="<?php print $viewImagePath; ?>" width="215px" height="300px">
        </div>
          <p class="anime-vertical-header">Episodes</p>
          <p class="anime-vertical-content"><?php print $episodes ?></p>
          <p class="anime-vertical-header">Type</p>
          <p class="anime-vertical-content"><?php print $type ?></p>
          <p class="anime-vertical-header">Start Date</p>
          <p class="anime-vertical-content"><?php print $airing_start ?></p>
          <p class="anime-vertical-header">Score</p>
          <p class="anime-vertical-content"><?php print $score ?></p>
          <p class="anime-vertical-header">Producers</p>
          <p class="anime-vertical-content"><?php print $producers ?></p>
          <p class="anime-vertical-header">Genres</p>
          <p class="anime-vertical-content"><?php print $genres ?></p>
          <p class="anime-vertical-header">URL</p>
          <p class="anime-vertical-content"><?php print "<a href=$url target=\"_blank\">$url</a>"; ?></p>
      </div>
      <div class="anime-information-container">
        <div>
          <p class="anime-information-title"><?php print $title ?></p>
          <p class="anime-information-synopsis"><?php print $synopsis ?></p>
        </div>

        <?php
          if($type === 'Movie') {
            print "
            <div>
            <p class=\"anime-information-title\">Overview</p>
            <iframe width=\"700\" height=\"400\" src=\"https://www.youtube.com/embed/$trailer_id\" >
            </iframe></div>";
          }
        ?>
      </div>
    </div>
  </div>
  </body>
</html>
