<?php require_once '../../controllers/animes/add_anime_controller.php'; ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Add Anime</title>
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
      <div class="container">
    <form method="post" enctype="multipart/form-data">
      <h2>Add Anime</h2>
      <?php require_once '../flash_message.php'; ?>
        <div class="field">
            <label>Title:</label>
            <input type="text" title="title" name="title" autocomplete="off" required></p>
        </div>

        <div class="field">
            <label>Genres</label><br>
            <div class="scrollbox">
              <label>Action
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Action"
                  ></label>
              <label>Adventure
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Adventure"
                  ></label>
              <label>Cars
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Cars"
                  ></label>
              <label>Comedy
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Comedy"
                  ></label>
              <label>Dementia
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Dementia"
                  ></label>
              <label>Demons
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Demons"
                  ></label>
              <label>Mystery
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Mystery"
                  ></label>
              <label>Drama
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Drama"
                  ></label>
              <label>Ecchi
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Ecchi"
                  ></label>
              <label>Fantasy
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Fantasy"
                  ></label>
              <label>Game
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Game"
                  ></label>
              <label>Hentai
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Hentai"
                  ></label>
              <label>Historical
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Historical"
                  ></label>
              <label>Horror
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Horror"
                  ></label>
              <label>Kvalues
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Kvalues"
                  ></label>
              <label>Magic
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Magic"
                  ></label>
              <label>Martial Arts
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Martial Arts"
                  ></label>
              <label>Mecha
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Mecha"
                  ></label>
              <label>Music
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Music"
                  ></label>
              <label>Parody
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Parody"
                  ></label>
              <label>Samurai
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Samurai"
                  ></label>
              <label>Romance
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Romance"
                  ></label>
              <label>School
                <input
                  type="checkbox"
                  name="genres[]"
                  value="School"
                  ></label>
              <label>Sci Fi
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Sci Fi"
                  ></label>
              <label>Shoujo
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Shoujo"
                  ></label>
              <label>Shoujo Ai
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Shoujo Ai"
                  ></label>
              <label>Shounen
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Shounen"
                  ></label>
              <label>Shounen Ai
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Shounen Ai"
                  ></label>
              <label>Space
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Space"
                  ></label>
              <label>Sports
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Sports"
                  ></label>
              <label>Super Power
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Super Power"
                  ></label>
              <label>Vampire
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Vampire"
                  ></label>
              <label>Yaoi
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Yaoi"
                  ></label>
              <label>Yuri
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Yuri"
                  ></label>
              <label>Harem
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Harem"
                  ></label>
              <label>Slice Of Life
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Slice Of Life"
                  ></label>
              <label>Supernatural
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Supernatural"
                  ></label>
              <label>Military
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Military"
                  ></label>
              <label>Police
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Police"
                  ></label>
              <label>Psychological
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Psychological"
                  ></label>
              <label>Thriller
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Thriller"
                  ></label>
              <label>Seinen
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Seinen"
                  ></label>
              <label>Josei
                <input
                  type="checkbox"
                  name="genres[]"
                  value="Josei"
                  ></label>
            </div>
        </div>

        <div class="field">
          <label for="type-select" required>Type:</label>
          <select name="type" id="type-select">
              <option disabled selected>Select type</option>
              <option value="Movie">Movie</option>
              <option value="TV">TV</option>
              <option value="OVA">OVA</option>
              <option value="Special">Special</option>
              <option value="ONA">ONA</option>
              <option value="Music">Music</option>
          </select>
        </div>

        <div class="field">
          <label>Airing start:</label>
          <input type="date" title="airing start" name="airing_start"  autocomplete="off" required>
        </div>

        <div class="field">
          <label>Producers:</label>
          <input type="text" title="producers" name="producers[]" autocomplete="off" required>
        </div>

        <div class="field">
          <label>Score (Optional):</label>
          <input type="number" name="score" step=".01" autocomplete="off">
        </div>

        <div class="field">
          <label>Episodes (Optional):</label>
          <input type="number" name="episodes" autocomplete="off">
        </div>

        <div class="field">
          <label>URL:</label>
          <input type="url" name="url" autocomplete="off">
        </div>

        <div class="field">
          <label for="synopsis">Synopsis:</label><br>
          <textarea name="synopsis" rows="8" cols="80" required></textarea>
        </div>

        <div class="field">
          <label>Image:</label>
          <input type="file" id="image" title="image" name="image" accept="image/*" required>
        </div>
        <div align="center">
          <button type="submit" name="submit" value="Submit">Submit</button>
          <a href="manage_animes.php">Back</a>
        </div>
    </form>
  </div>
  </body>
</html>
