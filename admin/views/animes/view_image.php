<?php
  require_once '../../../resources/bootstrap.php';
  require_once MODELS_PATH . 'Anime.php';

  validateSession();

  if(isset($_GET['image_id'])) {
    $anime = new Anime();
    $row = $anime->getImageById($_GET['image_id']);

    header("Content-type: " . $row["type"]);
    print $row['data'];
  }
