!(function(window, document) {
	'use strict';

	var addProducerButton = document.getElementById('add-producer');
	var producerInput = document.getElementById('producer-input');
	var typeSelect = document.getElementById('type-select');

	addProducerButton.addEventListener('click', function(){
		var inputHtml = "<p><input type=\"text\" title=\"producers\" name=\"producers[]\" autocomplete=\"off\" required></p>"
		producerInput.insertAdjacentHTML('afterend', inputHtml);
	});
})(this, document);
