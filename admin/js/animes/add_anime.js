!(function(window, document) {
	'use strict';

	var addProducerButton = document.getElementById('add-producer');
	var producerInput = document.getElementById('producer-input');
	var typeSelect = document.getElementById('type-select');

	addProducerButton.addEventListener('click', function(){
		var inputHtml = "<p><input type=\"text\" title=\"producers\" name=\"producers[]\" autocomplete=\"off\" required></p>"
		producerInput.insertAdjacentHTML('afterend', inputHtml);
	});

	if (typeSelect.value === 'Movie') {
		var inputHtml = "<p id=\"trailer-input\">Trailer URL (Optional): <input type=\"url\" name=\"trailer_url\"></p>";
		typeSelect.insertAdjacentHTML('afterend', inputHtml);
	} else {
		document.getElementById('trailer-input').remove();
	}

	typeSelect.onchange = function() {
		var selectedOption = this[this.selectedIndex];
    var selectedText = selectedOption.text;

	  if(selectedText === 'Movie') {
			var inputHtml = "<p id=\"trailer-input\">Trailer URL (Optional): <input type=\"url\" name=\"trailer_url\"></p>";
			typeSelect.insertAdjacentHTML('afterend', inputHtml);
		} else {
			document.getElementById('trailer-input').remove();
		}
	}

})(this, document);
