!(function(window, document) {
  'use strict';

  // Local variables

  // Local functions:
  function verifyCurrentPath() {
    if (window.location.href == 'http://127.0.0.1:5000/views/animes/manage_animes.php') {
      manageAnimeNav.style.color = '#0BA8FB';
      animeSymbol.style.color = '#0BA8FB';
      manageUserNav.style.color = 'black';
      usersSymbol.style.color = 'black';
    }

    if (window.location.href == 'http://127.0.0.1:5000/views/users/manage_users.php') {
      manageAnimeNav.style.color = 'black';
      animeSymbol.style.color = 'black';
      manageUserNav.style.color = '#0BA8FB';
      usersSymbol.style.color = '#0BA8FB';
    }
  }

  // Elements:
  var manageAnimeNav = document.getElementById('manage-animes');
  var manageUserNav = document.getElementById('manage-users');
  var animeSymbol = document.getElementById('anime-symbol');
  var usersSymbol = document.getElementById('users-symbol');

  // Event listeners:
  verifyCurrentPath()
})(this, document);
