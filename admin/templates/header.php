<header class="header">
<div class="header-top">
	<div class="home-area">
		<p class="home-label"><a class="home-label__content" href="<?php print URL_ROOT . 'views/home.php' ?>" title="Otacon">
			Otacon | Admin
    </a></p>
	</div>
	<nav>
	<ul class="nav">
		<li class="nav__item">
			<a href="<?php print URL_ROOT . 'views/animes/manage_animes.php'?>"  title="Manage Animes">
				<i id="anime-symbol" class="fa fa-play" aria-hidden="true">
					<span class="nav-item-title" id="manage-animes">Manage Animes</span>
				</i>
			</a>
		</li>
		<li class="nav__item">
			<a href="<?php print URL_ROOT . 'views/users/manage_users.php'?>" title="Manage Users">
				<i id="users-symbol" class="fa fa-users" aria-hidden="true">
			<span class="nav-item-title" id="manage-users">Manage Users</span>
		</i></a></li>
		<li>
			<a href="<?php print URL_ROOT . 'controllers/logout.php'?>" title="Logout" id="manage-users">
				<span class="logout" >Logout</span>
		  </a>
		</li>
	</ul>
	</nav>
</div>
</header>
