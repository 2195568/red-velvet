# Module 7
Server-Side Web Scripting

### How to use
- Download this setup [directly](https://gitlab.com/2195568/red-velvet/-/archive/master/red-velvet-master.zip) and extract it
````
# Move the project directory
mv /path/to/red-velvet /var/www
````

- Configuration Setup
````
# Copy the configuration files

cp /var/www/red-velvet/resources/red-velvet-public.conf /etc/apache2/sites-available

cp /var/www/red-velvet/resources/red-velvet-admin.conf /etc/apache2/sites-available



# Modify /etc/apache2/ports.conf file

# Add the following:
# Listen 4000
# Listen 5000


# Enabled sites

cd /etc/apache2/sites/available

a2ensite red-velvet-public.conf

a2ensite red-velvet-admin.conf

systemctl restart apache2
````

- Database Setup
````
# Access MySQL
mysql -u root -p < /var/www/red-velvet/resources/red-velvet.sql

CREATE USER 'module7'@'localhost';
GRANT ALL ON *.* TO 'module7'@'localhost' IDENTIFIED BY 'module777' WITH GRANT OPTION;
````

- Populating Database
````
cd /var/www/red-velvet/resources

php populate_anime.php
````
