<?php
    // Database Params
    define('DB_HOST','localhost');
    define('DB_USER','module7');
    define('DB_PASS','module777');
    define('DB_NAME','webtek');
    define('DB_PORT','3306');
    define('DB_CHARSET','utf8');

    // URL Root
    define('URL_ROOT', (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' .
      $_SERVER['HTTP_HOST'] . '/');

    // Resources Path
    define('RESOURCES_PATH', dirname(dirname(__FILE__)) . '/');
    define('MODELS_PATH', RESOURCES_PATH . 'models' . '/');
    define('TEMPLATES_PATH', RESOURCES_PATH . 'templates' . '/');
    define('HELPERS_PATH', RESOURCES_PATH . 'helpers' . '/');
    define('CONTROLLERS_PATH', RESOURCES_PATH . 'controllers' . '/');
