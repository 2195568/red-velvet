<?php
require_once 'bootstrap.php';
require_once MODELS_PATH . 'Anime.php';

$anime = new Anime();
$data = loadAnimeByGenre(5, 1);

$i = 0;
$time_pre = microtime(true);
foreach ($data as $value) {
    // verify duplication in the database
    $row = $anime->getAnimeByTitle($value['title']);

    if ($row !== false) {
        if ($row['title'] === $value['title']) {
            echo "\nSkipped [$i] Title => " . $value['title'] . "...\n";
            $i++;
            // don't push duplicate values
            continue;
        }
    }
    echo "\nGetting [$i] Title => " . $value['title'] . "...\n";

    // insert image in the database
    $imageData = file_get_contents($value['image_url']);
    $imageUrl = explode('.', $value['image_url']);
    $imageType = 'image/' . end($imageUrl);

    $anime->addImage(
        array(
        'type' => $imageType,
        'data' => $imageData
      )
    );

    $row = $anime->getLastImageId();
    $lastImageId = $row['id'];

    // add anime and image id
    $anime->addAnime(
        array(
        'title' => $value['title'],
        'type' => $value['type'],
        'synopsis' => $value['synopsis'],
        'genres' => getGenres($value['genres']),
        'producers' => getProducers($value['producers']),
        'score' => $value['score'],
        'airing_start' => date('Y-m-d', strtotime($value['airing_start'])),
        'episodes' => $value['episodes'],
        'image_id' => $lastImageId,
        'url' => $value['url'],
        'trailer_url' => $value['type'] === 'Movie' ? getTrailer($value['title']) : null
      )
    );
    $i++;
}
$time_post = microtime(true);
$exec_time = $time_post - $time_pre;
echo "\nDone! Finished in $exec_time seconds\n";

function loadAnimeByGenre($pageNumber, $genreId = 1)
{
  $anime = new Anime();

  $data = file_get_contents("https://api.jikan.moe/v3/genre/anime/$genreId/$pageNumber");
  $data =  json_decode($data, true);

  return $data['anime'];
}

function loadTop($pageNumber, $subType = '')
{
  $anime = new Anime();

  $data = file_get_contents("https://api.jikan.moe/v3/genre/anime/1/1");
  $data =  json_decode($data, true);
  $data = $data['anime'];
}

function getTrailer($title)
{
    $title = urlencode($title);
    $data = file_get_contents("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=$title+officialTrailer&key=AIzaSyDMq-jVsp10CcxfL6dr0HZS-W4g4BATyvw");
    $data = json_decode($data, true);
    $id = $data['items']['0']['id']['videoId'];
    print $id;
    if(!isset($id)) {
      return '';
    }
    if($id === null) {
      die('Bad api key');
    }
    return 'https://youtu.be/' . $id;
}

function getProducers($data)
{
    $result = '';

    foreach ($data as $value) {
        if (!empty($result)) {
            $result .= ',' . $value['name'];
        } else {
            $result .= $value['name'];
        }
    }
    return $result;
}

function getGenres($data)
{
    $result = '';

    foreach ($data as $value) {
        if (!empty($result)) {
            $result .= ',' . $value['name'];
        } else {
            $result .= $value['name'];
        }
    }
    return $result;
}

function printData($data)
{
    print_r($data);
}
