<?php

    class Database
    {
        private $host = DB_HOST;
        private $port = DB_PORT;
        private $user = DB_USER;
        private $password = DB_PASS;
        private $charset = DB_CHARSET;
        private $dbname = DB_NAME;
        private $statement;
        private $db;

        public function __construct()
        {
            try {
                $dsn = 'mysql:host=' . $this->host . ';port=' . $this->port . ';charset=' . $this->charset .
                    ';dbname=' . $this->dbname;

                $options = array(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->db = new PDO($dsn, $this->user, $this->password, $options);
            } catch (PDOException $exception) {
                error_log('Database error occured: ' . $exception->getMessage());
            }
        }

        public function query($sql)
        {
            $this->statement = $this->db->prepare($sql);
        }

        public function execute()
        {
            return $this->statement->execute();
        }

        public function executeWithParameter($inputParameters)
        {
            return $this->statement->execute($inputParameters);
        }

        public function resultSet()
        {
            $this->execute();
            return $this->statement->fetchAll();
        }

        public function getRow()
        {
            $row = $this->statement->fetch(PDO::FETCH_ASSOC);
            return $row;
        }
    }
