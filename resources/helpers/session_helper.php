<?php
    include_once 'redirect_helper.php';
    // Initialize session
    session_start();

    function logIn($username, $password)
    {
      $_SESSION['USERNAME'] = $username;
			$_SESSION['PASSWORD'] = $password;
    }

    function isLoggedIn()
    {
        if (isset($_SESSION['USERNAME']) && isset($_SESSION['PASSWORD'])) {
            return true;
        } else {
            return false;
        }
    }

    function validateSession()
    {
        if (!isLoggedIn()) {
            $root = URL_ROOT . 'index.php';
            $linkPath = URL_ROOT . 'css/style.css';
            $link = "<link rel=\"stylesheet\" href=\"$linkPath\">";
            $html = "
            <!DOCTYPE html>
            <html lang=\"en\" dir=\"ltr\">
            <header>
            $link
            </header>
            <body>
            <div class=\"flash fail\">
            <span>Session Expired. Please <a href=\"$root\">Login in</a> again.</span>
            </div>
            </body>
            </html>";
            die("$html");
            exit();
        }
    }
