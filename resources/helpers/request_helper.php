<?php

	function hasPostElement($value) {
		return isset($_POST[$value]) && !empty($_POST[$value]);
	}

	function hasGetElement($value) {
		return isset($_GET[$value]) && !empty($_GET[$value]);
	}
