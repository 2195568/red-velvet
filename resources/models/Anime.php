<?php
  class Anime
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database();
      }

      public function addAnime($data)
      {
          var_dump($data);
          $sql = 'INSERT INTO ' . DB_NAME . '.anime (title, type, synopsis, genres, producers, score, airing_start, episodes, image_id, url, trailer_url) VALUES (:title, :type, :synopsis, :genres, :producers, :score, :airing_start, :episodes, :image_id, :url, :trailer_url)';

          $this->db->query($sql);

          $inputParameters = array(
            ':title' => $data['title'],
            ':type' => $data['type'],
            ':synopsis' => $data['synopsis'],
            ':genres' => $data['genres'],
            ':producers' => $data['producers'],
            ':score' => $data['score'],
            ':airing_start' => $data['airing_start'],
            ':episodes' => $data['episodes'],
            ':image_id' => $data['image_id'],
            ':url' => $data['url'],
            ':trailer_url'  => $data['trailer_url']
          );

          $this->db->executeWithParameter($inputParameters);
      }

      public function addImage($data)
      {
          $sql = 'INSERT INTO '. DB_NAME . '.image (type, data) VALUES (:type, :data)';

          $this->db->query($sql);
          $inputParameters = array(
            ':type' => $data['type'],
            ':data' => $data['data']
          );

          $this->db->executeWithParameter($inputParameters);
      }

      public function updateAnime($id, $data)
      {
          $sql = 'UPDATE ' . DB_NAME . '.anime SET title = :title, type = :type, synopsis = :synopsis, genres = :genres, producers = :producers,
          score = :score, airing_start = :airing_start, episodes = :episodes,
          image_id = :image_id, url = :url, trailer_url  = :trailer_url
           WHERE id = :id';

           $this->db->query($sql);

          $inputParameters = array(
            ':id' => $id,
            ':title' => $data['title'],
            ':type' => $data['type'],
            ':synopsis' => $data['synopsis'],
            ':genres' => $data['genres'],
            ':producers' => $data['producers'],
            ':score' => $data['score'],
            ':airing_start' => $data['airing_start'],
            ':episodes' => $data['episodes'],
            ':image_id' => $data['image_id'],
            ':url' => $data['url'],
            ':trailer_url'  => $data['trailer_url']
          );

          $this->db->executeWithParameter($inputParameters);
      }

      public function deleteAnime($id)
      {
          $sql = 'DELETE FROM ' . DB_NAME . '.anime WHERE id = :id';
          $this->db->query($sql);

          $inputParameters = array(':id' => $id);
          $this->db->executeWithParameter($inputParameters);
      }

      public function getAnimeById($id)
      {
          $sql = 'SELECT * FROM ' . DB_NAME . '.anime WHERE id = :id';
          $this->db->query($sql);
          $inputParameters = array(':id' => $id);
          $this->db->executeWithParameter($inputParameters);
          return $this->db->getRow();
      }

      public function getImageById($id)
      {
          $sql = 'SELECT type, data FROM ' . DB_NAME . '.image WHERE id = :id';

          $this->db->query($sql);
          $inputParameter = array(':id' => $id);
          $this->db->executeWithParameter($inputParameter);

          return $this->db->getRow();
      }

      public function getAnimeByTitle($title)
      {
          $sql = 'SELECT * FROM ' . DB_NAME . '.anime WHERE title = :title';
          $this->db->query($sql);
          $inputParameter = array(':title' => $title);
          $this->db->executeWithParameter($inputParameter);
          return $this->db->getRow();
      }

      public function getAll()
      {
          $sql = 'SELECT * FROM ' . DB_NAME . '.anime';
          $this->db->query($sql);
          $this->db->execute();

          return $this->db->resultSet();
      }

      public function getLastImageId()
      {
        $sql = 'SELECT id FROM ' . DB_NAME . '.image ORDER BY id DESC LIMIT 1';
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->getRow();
      }

      public function getTopAnime($numberOfRows = 5, $type = '')
      {
        if(!empty($type)) {
          $sql = 'SELECT * FROM ' . DB_NAME . '.anime WHERE type = \'' . $type . '\' ORDER BY score DESC LIMIT ' . $numberOfRows;
        } else {
          $sql = 'SELECT * FROM ' . DB_NAME . '.anime ORDER BY score DESC LIMIT ' . $numberOfRows;
        }
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->resultSet();
      }

      public function getTopMovies($numberOfRows = 8)
      {
        $sql = 'SELECT * FROM ' . DB_NAME . '.anime WHERE type = \'Movie\' ORDER BY score DESC LIMIT ' . $numberOfRows;
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->resultSet();
      }

      public function getUpcomingAnime()
      {
        $sql = 'SELECT * FROM ' . DB_NAME . '.anime WHERE airing_start > NOW()';
        $this->db->query($sql);
        $this->db->execute();

        return $this->db->resultSet();
      }

      public function searchAnime($title, $type = 'Movie', $airingStatus = 'Completed', $genre = '', $sort = 'Ascending', $orderBy = 'title')
      {
        $airingStatus = empty($airingStatus) ? 'Completed' : $airingStatus;
        $type = empty($type) ? "" : "AND type = '$type'";
        $genre = empty($genre) ? "" : "AND genres LIKE '%$genre%'";
        $sort = $sort === 'Ascending' ? 'ASC' : 'DESC';
        $orderBy = empty($orderBy) ? 'title' : $orderBy;
        $orderByAndSort = "ORDER BY $orderBy $sort";

        if($airingStatus === 'Airing') {
          $twoYearsFromNow = date('Y') - 2;
          $airingDate = "AND EXTRACT(YEAR FROM airing_start) >= $twoYearsFromNow";
        } else if($airingStatus === 'Completed') {
          $threeYearsFromNow = date('Y') - 3;
          $airingDate = "AND EXTRACT(YEAR FROM airing_start) <= $threeYearsFromNow";
        } else {
          $dateToday = date('Y-m-d');
          $airingDate = "AND airing_start > $dateToday";
        }

        // search for the exact title;
        $sql = "SELECT * FROM " . DB_NAME . ".anime WHERE title = '$title' $type $genre $airingDate $orderByAndSort";
        $this->db->query($sql);
        $this->db->execute();

        // if there is no result then search for similar titles
        if(empty($this->db->resultSet())) {
          $sql = "SELECT * FROM " . DB_NAME . ".anime WHERE title LIKE '%$title%' $type $genre $airingDate $orderByAndSort";
          $this->db->query($sql);
          $this->db->execute();

          // if there is no still result then search only for similar title without additoinals params
          if(empty($this->db->resultSet())) {
            $sql = "SELECT * FROM " . DB_NAME . ".anime WHERE title LIKE '%$title%' $orderByAndSort";
            $this->db->query($sql);
            $this->db->execute();
          }
        }

        return $this->db->resultSet();
      }
  }
