<?php

  class User
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database();
      }

      public function addUser($data)
      {
          $sql = 'INSERT INTO ' . DB_NAME . '.user (username, password) VALUES (:username, :password)';
          $this->db->query($sql);

          // Bind values
          $inputParameters = array(
                    ':username' => $data['username'],
                    ':password' => $data['password']
                );

          // Execute
          $this->db->executeWithParameter($inputParameters);

          if ($this->db->getRow() !== false) {
              return true;
          } else {
              return false;
          }
      }

      public function updateUser($id, $data)
      {
          $sql = 'UPDATE ' . DB_NAME . '.user SET username = :username, password = :password WHERE id = :id';
          $this->db->query($sql);

          // Bind
          $inputParameters = array(
          ':id'  => $id,
          ':username' => $data['username'],
          ':password' => $data['password']
        );

          // Execute
          $this->db->executeWithParameter($inputParameters);
      }

      public function deleteUser($id)
      {
        $sql = 'DELETE FROM ' . DB_NAME . '.user WHERE id = :id';
        $this->db->query($sql);

        // Bind
        $inputParameters = array(':id' => $id);
        // Execute
        $this->db->executeWithParameter($inputParameters);

        return ($this->db->getRow() !== false);
      }

      public function getUserById($id)
      {
          $sql = 'SELECT * FROM ' . DB_NAME . '.user WHERE id = :id';
          $this->db->query($sql);
          $inputParameters = array(':id' => $id);
          $this->db->executeWithParameter($inputParameters);
          return $this->db->getRow();
      }

      public function getUserByName($name)
      {
          $sql = 'SELECT * FROM ' . DB_NAME . '.user WHERE username = :username';
          $this->db->query($sql);
          $inputParameters = array(':username' => $name);
          $this->db->executeWithParameter($inputParameters);
          return $this->db->getRow();
      }

      public function getAll()
      {
        $sql = 'SELECT * FROM ' . DB_NAME . '.user';
        $this->db->query($sql);
        $this->db->execute();
        return $this->db->resultSet();
      }
  }
