<?php
	// Load Config
	require_once 'config/config.php';

	// Load database
	require_once 'database/Database.php';

	// Load helpers
	require_once 'helpers/include_helper.php';
